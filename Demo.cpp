#include <iostream>
#include <string>
using namespace std;

string solution(string &S, int K)
{
    // write your code in C++14 (g++ 6.2.0)
    string T, U;
    int i = 0, j = 0;
    bool b = true;
    for (i = 0; i < S.size(); i++)
    {
        if (S[i] != '-')
        {
            if (S[i] >= 97 && S[i] <= 122)
            {
                T.push_back(S[i] - 32);
            }
            else
            {
                T.push_back(S[i]);
            }
        }
    }
    // printf("%s", T.c_str());
    int n = T.size(), r = n % K;
    for (i = 0, j = 1; i < n; i++, j++)
    {
        U.push_back(T[i]);
        if (((j == r && b) || j == K) && i < n - 1)
        {
            printf("%d\n", j);
            U.push_back('-');
            j = 0;
            b = false;
        }
    }
    return U;
}

int main()
{
    string S = "Hello-0qwd-efjfjd-2ezx";
    printf("%s\n", solution(S, 4).c_str());
    return 0;
}