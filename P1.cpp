#include <iostream>
#include <unordered_set>
#include <vector>
using namespace std;

int main()
{
    int sum = 0, num, i;
    unordered_set<int> m;
    vector<int> v;
    FILE *fptr;
    fptr = fopen("input2", "r");
    while (fscanf(fptr, "%d\n", &num) != EOF)
    {
        v.push_back(num);
    }
    fclose(fptr);
    m.insert(0);
    for (i = 0;; i = (i + 1) % v.size())
    {
        sum = sum + v[i];
        if (m.find(sum) != m.end())
        {
            printf("%d", sum);
            break;
        }
        m.insert(sum);
    }
    return 0;
}