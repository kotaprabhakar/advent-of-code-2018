#include <iostream>
using namespace std;

struct seq
{
    char a;
    seq *next;
    seq()
    {
        a = '\0';
        next = NULL;
    }
};

int main()
{
    FILE *fptr = fopen("input5", "r");
    char line[50001];
    int i = 0, cnt = 1, ans = 0;
    seq *first, *head, *prev, *temp, *iter;
    fgets(line, sizeof(line), fptr);
    head = new seq();
    head->a = line[0];
    first = new seq();
    first->next = head;
    prev = head;
    for (i = 1; i < strlen(line); i++)
    {
        temp = new seq();
        temp->a = line[i];
        prev->next = temp;
        prev = temp;
    }

    while (cnt != 0)
    {
        cnt = 0;
        for (iter = first; iter != NULL && iter->next != NULL && iter->next->next != NULL;)
        {
            if (iter->next->a == iter->next->next->a + 32 ||
                iter->next->a == iter->next->next->a - 32)
            {
                temp = iter->next->next->next;
                delete iter->next;
                delete iter->next->next;
                iter->next = temp;
                cnt++;
            }
            else
            {
                iter = iter->next;
            }
        }
    }
    temp = head;
    while (temp != NULL)
    {
        ans++;
        temp = temp->next;
    }
    printf("%d\n", ans);
    for (temp = head; temp != NULL; temp = prev)
    {
        prev = temp->next;
        delete temp;
    }
    delete first;
    fclose(fptr);
    return 0;
}