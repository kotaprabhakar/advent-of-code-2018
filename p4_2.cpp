#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

int main()
{
    FILE *fptr;
    vector<string> lines;
    unordered_map<int, vector<int>> sleep_durations;
    fptr = fopen("input4", "r");
    char line[200];
    int guard_number, sleep_duration, start, end, max_min = INT_MIN, ans;
    while (fgets(line, sizeof(line), fptr))
    {
        lines.push_back(string(line));
    }
    fclose(fptr);
    sort(lines.begin(), lines.end(), [](const string &x1, const string &x2) -> bool {
        int dd1 = atoi(x1.substr(9, 2).c_str()), dd2 = atoi(x2.substr(9, 2).c_str());
        int mm1 = atoi(x1.substr(6, 2).c_str()), mm2 = atoi(x2.substr(6, 2).c_str());
        int mn1 = atoi(x1.substr(15, 2).c_str()), mn2 = atoi(x2.substr(15, 2).c_str());
        int hr1 = atoi(x1.substr(12, 2).c_str()), hr2 = atoi(x2.substr(12, 2).c_str());
        if (mm1 < mm2)
            return true;
        if (mm1 == mm2 && dd1 < dd2)
            return true;
        if (mm1 == mm2 && dd1 == dd2 && hr1 < hr2)
            return true;
        if (mm1 == mm2 && dd1 == dd2 && hr1 == hr2 && mn1 < mn2)
            return true;
        return false;
    });
    for (auto l : lines)
    {
        char d1[200], d2[200];
        if (l[19] == 'G')
        {
            sscanf(l.c_str(), "%s %s Guard #%d begins shift\n", d1, d2, &guard_number);
            sleep_durations.insert({guard_number, vector<int>(60, 0)});
        }
        else if (l[19] == 'f')
        {
            start = atoi(l.substr(15, 2).c_str());
        }
        else if (l[19] == 'w')
        {
            end = atoi(l.substr(15, 2).c_str());
            for (int i = start; i <= end; i++)
            {
                sleep_durations[guard_number][i]++;
            }
        }
    }
    auto max_min_guard =
        max_element(sleep_durations.begin(), sleep_durations.end(),
                    [](const pair<int, vector<int>> &p1, const pair<int, vector<int>> &p2)
                        -> bool {
                        int max1 = INT_MIN, max2 = INT_MIN;
                        for (int i = 0; i < 60; i++)
                        {
                            if ((p1.second)[i] > max1)
                            {
                                max1 = (p1.second)[i];
                            }
                            if ((p2.second)[i] > max2)
                            {
                                max2 = (p2.second)[i];
                            }
                        }
                        return max1 < max2;
                    });
    int max1 = INT_MIN, index1;
    for (int i = 0; i < 60; i++)
    {
        if ((max_min_guard->second)[i] > max1)
        {
            max1 = (max_min_guard->second)[i];
            index1 = i;
        }
    }
    printf("%d\n", index1 * max_min_guard->first);

    return 0;
}