#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

int main()
{
    FILE *fptr;
    vector<string> lines;
    vector<int> arr(60, 0);
    unordered_map<int, int> sleep_durations;
    fptr = fopen("input4", "r");
    char line[200];
    int guard_number, sleep_duration, start, max_min = INT_MIN, ans;
    while (fgets(line, sizeof(line), fptr))
    {
        lines.push_back(string(line));
    }
    fclose(fptr);
    sort(lines.begin(), lines.end(), [](const string &x1, const string &x2) -> bool {
        int dd1 = atoi(x1.substr(9, 2).c_str()), dd2 = atoi(x2.substr(9, 2).c_str());
        int mm1 = atoi(x1.substr(6, 2).c_str()), mm2 = atoi(x2.substr(6, 2).c_str());
        int mn1 = atoi(x1.substr(15, 2).c_str()), mn2 = atoi(x2.substr(15, 2).c_str());
        int hr1 = atoi(x1.substr(12, 2).c_str()), hr2 = atoi(x2.substr(12, 2).c_str());
        if (mm1 < mm2)
            return true;
        if (mm1 == mm2 && dd1 < dd2)
            return true;
        if (mm1 == mm2 && dd1 == dd2 && hr1 < hr2)
            return true;
        if (mm1 == mm2 && dd1 == dd2 && hr1 == hr2 && mn1 < mn2)
            return true;
        return false;
    });

    for (auto l : lines)
    {
        char d1[200], d2[200];
        if (l[19] == 'G')
        {
            sscanf(l.c_str(), "%s %s Guard #%d begins shift\n", d1, d2, &guard_number);
            sleep_durations.insert({guard_number, 0});
        }
        else if (l[19] == 'f')
        {
            sleep_duration = atoi(l.substr(15, 2).c_str());
        }
        else if (l[19] == 'w')
        {
            sleep_durations[guard_number] += (atoi(l.substr(15, 2).c_str()) - sleep_duration);
        }
    }

    auto max_pair = max_element(sleep_durations.begin(), sleep_durations.end(),
                                [](const pair<int, int> &p1, const pair<int, int> &p2)
                                    -> bool {
                                    return p1.second < p2.second;
                                });
    for (auto l : lines)
    {
        char d1[200], d2[200];
        if (l[19] == 'G')
        {
            sscanf(l.c_str(), "%s %s Guard #%d begins shift\n", d1, d2, &guard_number);
        }
        if (guard_number == max_pair->first && l[19] == 'f')
        {
            start = atoi(l.substr(15, 2).c_str());
        }
        if (guard_number == max_pair->first && l[19] == 'w')
        {
            int end = atoi(l.substr(15, 2).c_str());
            for (int i = start; i <= end; i++)
            {
                arr[i]++;
            }
        }
    }

    for (int i = 0; i < 60; i++)
    {
        if (arr[i] > max_min)
        {
            max_min = arr[i];
            ans = i;
        }
    }

    printf("%d\n", max_pair->first * ans);

    return 0;
}