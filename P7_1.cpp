#include <algorithm>
#include <iostream>
#include <unordered_set>
#include <vector>
using namespace std;

int main()
{
    FILE *fptr = fopen("input7", "r");
    char x, y;
    int i = 0, j = 0;
    string res;
    vector<vector<bool>> gr(26, vector<bool>(26, false));
    vector<char> q;
    vector<bool> alphabet(26, false);
    while (fscanf(fptr, "Step %c must be finished before step %c can begin.\n", &x, &y) != EOF)
    {
        gr[y - 65][x - 65] = true;
    }

    for (auto v : gr)
    {
        bool x = false;
        for (auto w : v)
        {
            if (w)
            {
                x = true;
                break;
            }
        }
        if (!x)
            q.push_back(i + 65);
        i++;
    }

    sort(q.begin(), q.end(), [](const char &o1, const char &o2) {
        return o1 > o2;
    });

    while (!q.empty())
    {
        auto curr = q.back();
        alphabet[curr - 65] = true;
        res.push_back(curr);
        q.pop_back();
        for (i = 0; i < 26; i++)
        {
            if (gr[i][curr - 65])
            {
                bool ch = false;
                for (j = 0; j < 26; j++)
                {
                    if (gr[i][j] && !alphabet[j])
                    {
                        ch = true;
                        break;
                    }
                }
                if (!ch && !alphabet[i])
                    q.push_back(i + 65);
            }
        }
        sort(q.begin(), q.end(), [](const char &o1, const char &o2) {
            return o1 > o2;
        });
    }

    printf("%s\n", res.c_str());

    fclose(fptr);
    return 0;
}