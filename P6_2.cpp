#include <iostream>
#include <limits.h>
#include <unordered_map>
#include <vector>
using namespace std;

int main() {
  FILE *fptr = fopen("input6_sample", "r");
  vector<pair<int, int>> coords;
  int x, y, x_min = INT_MAX, x_max = INT_MIN, y_min = INT_MAX, y_max = INT_MIN,
            i = 1, j, k = 0, ans = INT_MIN;
  while (fscanf(fptr, "%d, %d\n", &x, &y) != EOF) {
    coords.push_back({x, y});
    x_min = min(x_min, x);
    x_max = max(x_max, x);
    y_min = min(y_min, y);
    y_max = max(y_max, y);
  }
  fclose(fptr);
  vector<vector<int>> cartesian_plane(x_max - x_min + 1,
                                      vector<int>(y_max - y_min + 1, -1));
  for (auto coord : coords) {
    cartesian_plane[coord.first - x_min][coord.second - y_min] = k++;
  }
  for (i = 0; i < cartesian_plane.size(); i++) {
    for (j = 0; j < cartesian_plane[i].size(); j++) {
      int sum = 0;
      for (auto coord : coords) {
        sum += abs(coord.first - x_min - i) + abs(coord.second - y_min - j);
      }
      if (sum < 32) {
        k++;
      }
    }
  }
  printf("%d\n", k);
  return 0;
}
