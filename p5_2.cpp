#include <iostream>
using namespace std;

struct seq
{
    char a;
    seq *next;
    seq()
    {
        a = '\0';
        next = NULL;
    }
};

int reduce(seq *first)
{
    seq *iter, *temp;
    int cnt = 1, ans = 0;
    while (cnt != 0)
    {
        cnt = 0;
        for (iter = first; iter != NULL && iter->next != NULL && iter->next->next != NULL;)
        {
            if (iter->next->a == iter->next->next->a + 32 ||
                iter->next->a == iter->next->next->a - 32)
            {
                temp = iter->next->next->next;
                delete iter->next;
                delete iter->next->next;
                iter->next = temp;
                cnt++;
            }
            else
            {
                iter = iter->next;
            }
        }
    }
    iter = first->next;
    while (iter != NULL)
    {
        ans++;
        iter = iter->next;
    }
    return ans;
}

int sequencify(char (&line)[50001], char x)
{
    seq *first = new seq(), *temp, *prev, *iter;
    prev = first;
    int ans = 0;
    for (int i = 0; i < 50000; i++)
    {
        if (line[i] != x && line[i] != x - 32)
        {
            temp = new seq();
            temp->a = line[i];
            prev->next = temp;
            prev = prev->next;
        }
    }
    ans = reduce(first);
    iter = first->next;
    while (iter != NULL)
    {
        temp = iter->next;
        delete iter;
        iter = temp;
    }

    delete first;
    return ans;
}

int main()
{
    FILE *fptr = fopen("input5", "r");
    char line[50001], iter = 'a';
    int minVal = INT_MAX;
    fgets(line, sizeof(line), fptr);
    for (iter = 'a'; iter <= 'z'; iter++)
    {
        minVal = min(minVal, sequencify(line, iter));
    }
    printf("%d\n", minVal);
    fclose(fptr);
    return 0;
}