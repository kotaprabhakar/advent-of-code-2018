#include <iostream>
#include <limits.h>
#include <unordered_map>
#include <vector>
using namespace std;

int main() {
  FILE *fptr = fopen("input6", "r");
  vector<pair<int, int>> coords;
  int x, y, x_min = INT_MAX, x_max = INT_MIN, y_min = INT_MAX, y_max = INT_MIN,
            i = 1, j, k = 1, ans = INT_MIN;
  unordered_map<int, int> m;
  while (fscanf(fptr, "%d, %d\n", &x, &y) != EOF) {
    coords.push_back({x, y});
    x_min = min(x_min, x);
    x_max = max(x_max, x);
    y_min = min(y_min, y);
    y_max = max(y_max, y);
  }
  fclose(fptr);
  vector<vector<int>> cartesian_plane(x_max - x_min + 1,
                                      vector<int>(y_max - y_min + 1, -1));
  for (auto coord : coords) {
    cartesian_plane[coord.first - x_min][coord.second - y_min] = k++;
  }
  for (i = 0; i < cartesian_plane.size(); i++) {
    for (j = 0; j < cartesian_plane[i].size(); j++) {
      int min_value = INT_MAX, min_val_coord = -1;
      k = 1;
      for (auto coord : coords) {
        if (abs(coord.first - x_min - i) + abs(coord.second - y_min - j) <
            min_value) {
          min_value =
              abs(coord.first - x_min - i) + abs(coord.second - y_min - j);
          min_val_coord = k;
        } else if (abs(coord.first - x_min - i) +
                       abs(coord.second - y_min - j) ==
                   min_value) {
          min_val_coord = -1;
        }
        k++;
      }
      cartesian_plane[i][j] = min_val_coord;
      if (m.find(min_val_coord) == m.end()) {
        m.insert({min_val_coord, 1});
      } else {
        m[min_val_coord]++;
      }
    }
  }

  for (auto iter : m) {
    ans = max(ans, iter.second);
  }
  printf("%d\n", ans);
  return 0;
}
