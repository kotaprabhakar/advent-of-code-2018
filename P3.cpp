#include <iostream>
#include <vector>
using namespace std;

int main()
{
    FILE *fptr;
    fptr = fopen("input3", "r");
    vector<vector<int>> v(1000, vector<int>(1000, 0));
    int idx, x, y, w, h, area = 0, x_min = INT_MAX, x_max = INT_MIN, y_min = INT_MAX, y_max = INT_MIN;
    int i, j, c;
    while (fscanf(fptr, "#%d @ %d,%d: %dx%d\n", &idx, &x, &y, &w, &h) != EOF)
    {
        for (i = x; i < x + w; i++)
        {
            for (j = y; j < y + h; j++)
            {
                if (v[i][j] == 1)
                {
                    area++;
                }
                v[i][j]++;
            }
        }
    }
    fseek(fptr, 0, SEEK_SET);
    while (fscanf(fptr, "#%d @ %d,%d: %dx%d\n", &idx, &x, &y, &w, &h) != EOF)
    {
        c = 0;
        for (i = x; i < x + w; i++)
        {
            for (j = y; j < y + h; j++)
            {
                if (v[i][j] != 1)
                {
                    c = 1;
                }
            }
        }
        if (c == 0)
        {
            printf("%d\n", idx);
        }
    }
    // printf("%d\n", area);
    fclose(fptr);
    return 0;
}
