#include <iostream>
#include <vector>
using namespace std;

#define SIZE 100
#define ALPHABET 26

bool difference_is_one(string s1, string s2)
{
    int i, c = 0;
    for (i = 0; i < s1.size(); i++)
    {
        if (s1[i] != s2[i])
            c++;
    }
    return c == 1;
}

int main()
{
    FILE *fptr;
    char phrase[SIZE];
    fptr = fopen("input_puzzle2", "r");
    vector<string> v;
    int i;
    while (fscanf(fptr, "%s", phrase) != EOF)
    {
        v.push_back(string(phrase));
    }
    sort(v.begin(), v.end());
    for (i = 0; i < v.size() - 1; i++)
    {
        if (difference_is_one(v[i], v[i + 1]))
        {
            printf("%s %s", v[i].c_str(), v[i + 1].c_str());
            break;
        }
    }
    fclose(fptr);
    return 0;
}